#include <random>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <drogon/drogon.h>

using namespace drogon;

static const int PORT = 8080;
static const int BUFSIZE = 128;
static const std::string VERSION = "1.2-c++";

std::string execute_command(const char *cmd)
{
    std::string result = "";
    std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if (!pipe)
    {
        return result;
    }
    char buffer[BUFSIZE] = {0};
    while (!feof(pipe.get()))
    {
        if (fgets(buffer, BUFSIZE, pipe.get()) != NULL)
            result += buffer;
    }
    return result;
}

int random_number()
{
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist(1, 1000);
    return dist(rng);
}

std::string respond_text(const char *fortune, int random)
{
    std::stringstream ss;
    ss << "Fortune " << VERSION << " cookie of the day #" << random << ":\n\n" << fortune;
    return ss.str();
}

// tag::router[]
int main()
{
    std::string hostname = execute_command("hostname");
    std::string version = std::string(VERSION);
    auto router = [=](const HttpRequestPtr &req,
                      std::function<void(const HttpResponsePtr &)> &&callback)
    {
        std::string fortune = execute_command("fortune");
        int number = random_number();

        std::string accept = req->getHeader("Accept");
        if (accept == "application/json")
        {
            Json::Value json;
            json["hostname"] = hostname;
            json["message"] = fortune;
            json["number"] = number;
            json["version"] = version;
            auto resp = HttpResponse::newHttpJsonResponse(json);
            callback(resp);
            return;
        }
        if (accept == "text/plain")
        {
            std::string result = respond_text(fortune.c_str(), number);
            auto resp = HttpResponse::newHttpResponse();
            resp->setBody(result);
            resp->setContentTypeString("text/plain");
            callback(resp);
            return;
        }
        HttpViewData data;
        data.insert("fortune", fortune);
        data.insert("number", std::to_string(number));
        data.insert("version", version);
        data.insert("hostname", hostname);
        auto resp = HttpResponse::newHttpViewResponse("fortune", data);
        callback(resp);
    };
    app().registerHandler("/", router).addListener("0.0.0.0", PORT).run();
    return EXIT_SUCCESS;
}
// end::router[]

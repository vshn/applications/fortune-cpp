# Step 1: Builder image
FROM debian:stable-slim AS builder

# Install build requirements
RUN apt-get update && apt-get install -y build-essential cmake zip unzip curl git tar pkg-config bison fortune

# Install vcpkg and Drogon
RUN git clone https://github.com/Microsoft/vcpkg.git /vcpkg
WORKDIR /vcpkg
RUN ./bootstrap-vcpkg.sh
RUN ./vcpkg integrate install
RUN ./vcpkg install drogon\[core,ctl\]:x64-linux

# Build application
WORKDIR /builder
ENV PATH=$PATH:/vcpkg/installed/x64-linux/tools/drogon
COPY CMakeLists.txt .
COPY src /builder/src
COPY views /builder/views
RUN cmake -B build -S . -DCMAKE_TOOLCHAIN_FILE=/vcpkg/scripts/buildsystems/vcpkg.cmake
RUN cmake --build build

# tag::production[]
# Step 2: Production image
FROM busybox:glibc
COPY --from=builder /builder/build/cpp-fortune /usr/local/bin/cpp-fortune
COPY --from=builder /usr/games/fortune /usr/local/bin/fortune
COPY --from=builder /usr/share/games/fortunes /usr/share/games/fortunes
COPY --from=builder /lib/x86_64-linux-gnu/libdl.so.2 /lib/x86_64-linux-gnu/libdl.so.2
COPY --from=builder /lib/x86_64-linux-gnu/libgcc_s.so.1 /lib/x86_64-linux-gnu/libgcc_s.so.1
COPY --from=builder /usr/lib/x86_64-linux-gnu/libstdc++.so.6 /usr/lib/x86_64-linux-gnu/libstdc++.so.6
COPY --from=builder /usr/lib/x86_64-linux-gnu/librecode.so.0 /usr/lib/x86_64-linux-gnu/librecode.so.0

EXPOSE 8080

# <1>
USER 1001:0

CMD ["/usr/local/bin/cpp-fortune"]
# end::production[]
